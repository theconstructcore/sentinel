import yaml
import sys

def generate_numberelementsfile(num_elements, num_elements_filename):

    data = [str(num_elements)]
    fill_file_with_data(data, num_elements_filename)


def generate_controllerargs_dict(num_elements,number_tentacles):
    """
    Generate All the controle arguments based on the number of elements
    """
    
    data = []
    for tentacle_num in range(1, number_tentacles+1):
        
        base_name = "t"+str(tentacle_num)
    
        for x in range(0, num_elements):
            aux_controller_name = base_name+"_"+str(x)+"_aux_joint_position_controller"
            main_controller_name = base_name+"_"+str(x)+"_joint_position_controller"
            # We add the new connector element controllers
            aux_connector_controller_name = base_name+"_"+str(x)+"_aux_connector_joint_position_controller"
            main_connector_controller_name = base_name+"_"+str(x)+"_connector_joint_position_controller"
            
            data.append(aux_controller_name)
            data.append(main_controller_name)
            # We add the new connector element controllers
            data.append(aux_connector_controller_name)
            data.append(main_connector_controller_name)
        
        # Add TCP controllers for each tentacle
        for finguer_number in range(1,5):
            f_tip_controller_name = base_name+"_claw_finguer_tip_"+str(finguer_number)+"_joint_position_controller"
            f_base_controller_name = base_name+"_claw_finguer_base_"+str(finguer_number)+"_joint_position_controller"
            data.append(f_tip_controller_name)
            data.append(f_base_controller_name)
        
    data.append("joint_state_controller")
    data.append("--shutdown-timeout 3")
    
    return data


def generate_controllerconfig_yaml_dict(num_elements,number_tentacles):
    """
    Generate the dict for the yaml config file for the controle
    
    """
    name_space = 'tentacle'
    publish_rate = 50
    

    """
    data = {name_space:
                {   'joint_state_controller':{'type':'joint_state_controller/JointStateController',
                                            'publish_rate':publish_rate
                                            },
                    'snake_body_0_aux_joint_position_controller':{  'type':'effort_controllers/JointPositionController',
                                                                    'joint':'snake_body_0.0_aux_joint',
                                                                    'pid':dict(p = 3.0, i = 1.0, d = 0.0)
                                                                },
                    'snake_body_0_joint_position_controller':{  'type':'effort_controllers/JointPositionController',
                                                                'joint':'snake_body_0.0_aux_joint',
                                                                'pid':dict(p = 3.0, i = 1.0, d = 0.0)
                                                                },
                    
                }
            }
            
    """
    
    content_dict = {   'joint_state_controller':{'type':'joint_state_controller/JointStateController',
                                            'publish_rate':publish_rate
                                            },
                    }
                    
    
    for tentacle_num in range(1, number_tentacles+1):
        
        base_name = "t"+str(tentacle_num)
    
        for x in range(0, num_elements):
            # TODO: Fixing wrong joint nameing due to float format instead of int.
            #float_string_value = str(float(x))
            float_string_value = str(int(x))
            int_string_value = str(x)
            
            aux_controller_name = base_name+'_'+int_string_value+'_aux_joint_position_controller'
            
            aux_controller_data_dict = {'type':'effort_controllers/JointPositionController',
                                    'joint':base_name+"_"+float_string_value+"_aux_joint",
                                    'pid':dict(p = 3.0, i = 1.0, d = 0.0)
                                    }
                                    
            controller_name = base_name+'_'+int_string_value+'_joint_position_controller'
            
            controller_data_dict = {'type':'effort_controllers/JointPositionController',
                                    'joint':base_name+"_"+float_string_value+"_joint",
                                    'pid':dict(p = 3.0, i = 1.0, d = 0.0)
                                    }
            
            
            # We add the new connector element controllers
            aux_connector_controller_name = base_name+'_'+int_string_value+'_aux_connector_joint_position_controller'
            
            aux_connector_controller_data_dict = {'type':'effort_controllers/JointPositionController',
                                    'joint':base_name+"_"+float_string_value+"_aux_connector_joint",
                                    'pid':dict(p = 3.0, i = 1.0, d = 0.0)
                                    }
                                    
            connector_controller_name = base_name+'_'+int_string_value+'_connector_joint_position_controller'
            
            connector_controller_data_dict = {'type':'effort_controllers/JointPositionController',
                                    'joint':base_name+"_"+float_string_value+"_connector_joint",
                                    'pid':dict(p = 3.0, i = 1.0, d = 0.0)
                                    }
            
            
            content_dict[aux_controller_name] = aux_controller_data_dict
            content_dict[controller_name] = controller_data_dict
            # We add the new connector element controllers
            content_dict[aux_connector_controller_name] = aux_connector_controller_data_dict
            content_dict[connector_controller_name] = connector_controller_data_dict
        
        # We add for TCP controle
        for finguer_number in range(1,5):
            f_tip_controller_name = base_name+"_claw_finguer_tip_"+str(finguer_number)+"_joint_position_controller"
            f_tip_joint_name = base_name+"_claw_finguer_tip_"+str(finguer_number)+"_joint"
            f_tip_controller_data_dict = {'type':'effort_controllers/JointPositionController',
                                        'joint':f_tip_joint_name,
                                        'pid':dict(p = 3.0, i = 1.0, d = 0.0)
                                        }
            
            f_base_controller_name = base_name+"_claw_finguer_base_"+str(finguer_number)+"_joint_position_controller"
            f_base_joint_name = base_name+"_claw_finguer_base_"+str(finguer_number)+"_joint"
            f_base_controller_data_dict = {'type':'effort_controllers/JointPositionController',
                                        'joint':f_base_joint_name,
                                        'pid':dict(p = 3.0, i = 1.0, d = 0.0)
                                        }
            
            content_dict[f_tip_controller_name] = f_tip_controller_data_dict
            content_dict[f_base_controller_name] = f_base_controller_data_dict
    
    data = {name_space:content_dict}
            

    return data
    

def generate_controllerargsfile(num_elements, controllerargs_filename, num_tentacles):


    data = generate_controllerargs_dict(num_elements,num_tentacles)
    fill_file_with_data(data, controllerargs_filename)
    
def generate_controllerconfig_yamlfile(num_elements, controllerconfig_yaml_filename, num_tentacles):

    data = generate_controllerconfig_yaml_dict(num_elements,num_tentacles)
    fill_yaml_with_datadict(data, controllerconfig_yaml_filename)
    

def fill_file_with_data(data, filename):
    
    with open(filename, 'w') as outfile:
        for element in data:
            data_to_write = element + "\n"
            outfile.write(data_to_write)    
            
def fill_yaml_with_datadict(data, yamlfilename):
    
    with open(yamlfilename, 'w') as yaml_file:
        yaml.dump(data, yaml_file, default_flow_style=False) 

if __name__ == "__main__":
    
    
    
    num_elements = int(sys.argv[1])
    num_tentacles = int(sys.argv[2])
    print(str(num_elements))
    
    generate_numberelementsfile(num_elements=num_elements, num_elements_filename='numelements_temp.yaml')
    generate_controllerargsfile(num_elements=num_elements, controllerargs_filename='controllerargs_temp.yaml', num_tentacles=num_tentacles)
    generate_controllerconfig_yamlfile(num_elements=num_elements, controllerconfig_yaml_filename='sentinel_full_control_temp.yaml', num_tentacles=num_tentacles)
    
