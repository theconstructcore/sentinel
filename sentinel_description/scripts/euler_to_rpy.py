#!/usr/bin/env python
import rospy
from nav_msgs.msg import Odometry
from tf.transformations import euler_from_quaternion, quaternion_from_euler

rospy.init_node('my_quaternion_to_euler')

roll=1.339264439
pitch=-1.084709913
yaw=-0.57107173

orientation_q = quaternion_from_euler (roll, pitch,yaw)
print orientation_q
orientation_list = [orientation_q[0], orientation_q[1], orientation_q[2], orientation_q[3]]
(roll, pitch, yaw) = euler_from_quaternion (orientation_list)
print str(roll)+","+str(pitch)+","+str(yaw)

rospy.spin()