#!/usr/bin/env python

import sys
import rospkg
import os
import math

# Position of 17 Tentacles
TENTACLE_POS_X_ARRAY = [0.01,0.15,-0.15,0.23,-0.23,0.125,-0.125,0.125,-0.125,0.255,-0.255,0.25,-0.25,0.15,-0.15,0.05,-0.05]
TENTACLE_POS_Y_ARRAY = [-0.03,-0.15,-0.15,-0.065,-0.065,0.0,0.0,0.09,0.09,0.063,0.063,0.17,0.17,0.2,0.2,0.2,0.2]


def generate_tentacle_files(num_tentacles):
    
    
    # get an instance of RosPack with the default search paths
    rospack = rospkg.RosPack()
    # get the file path for rospy_tutorials
    base_package_path = rospack.get_path('sentinel_description')
    base_robot_dir_path = os.path.join(base_package_path,"robot")
    
    file_template = "tentacleX.urdf.xacro"
    file_template_path = os.path.join(base_robot_dir_path,file_template)
    
    for tentacle_num in range(1, number_tentacles+1):
        
        # Read in the file
        with open(file_template_path, 'r') as file :
            filedata = file.read()
        
        # Replace tentacle number
        filedata = filedata.replace('TENTACLENUM', str(tentacle_num))
        
        # Replace SIN and COS Values
        alfa = 2 * math.pi * (float(tentacle_num) / float(number_tentacles))
        sin_value = math.sin(alfa)
        cos_value = math.cos(alfa)
        
        
        #filedata = filedata.replace('SIN_ALFA', str(sin_value))
        #filedata = filedata.replace('COS_ALFA', str(cos_value))
        
        tentacle_pos_x = TENTACLE_POS_X_ARRAY[tentacle_num-1]
        tentacle_pos_y = TENTACLE_POS_Y_ARRAY[tentacle_num-1]
        
        filedata = filedata.replace('TENTACLEPOSX', str(tentacle_pos_x))
        filedata = filedata.replace('TENTACLEPOSY', str(tentacle_pos_y))
        
        # Write the file out again
        new_file_name = "tentacle"+str(tentacle_num)+".urdf.xacro"
        new_file_path = os.path.join(base_robot_dir_path,new_file_name)
        with open(new_file_path, 'w') as file:
            file.write(filedata)
        print "Creating Tentacle Xacro file ==>"+str(new_file_path)
  
if __name__ == "__main__":
    
    number_tentacles = int(sys.argv[1])
    generate_tentacle_files(number_tentacles)