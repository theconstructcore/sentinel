#!/usr/bin/env python
import sys
import rospy
import time
from math import pi, sin, cos, acos
import random
from std_msgs.msg import Float64



class SentinelJointMover(object):

    def __init__(self, number_tentacles, number_element_each_tentacle):
        rospy.init_node('sentinel_jointmover', anonymous=True)
        rospy.loginfo("Sentinel Initialising...")

        self._number_tentacles = number_tentacles
        self._number_element_each_tentacle = number_element_each_tentacle
        
        self.init_tentacle_pub_array()
        self._rate = rospy.Rate(5)

    def init_tentacle_pub_array(self):
        
        self._tentacle_pub_array = []
        
        # /tentacle/t2_2_joint_position_controller/command
        namespace = "/tentacle/"
        ending = "/command"
        
        for tentacle_num in range(1, self._number_tentacles+1):
        
            base_name = "t"+str(tentacle_num)
    
            for x in range(0, self._number_element_each_tentacle):
                
                aux_controller_topic = namespace+base_name+"_"+str(x)+"_aux_joint_position_controller"+ending
                main_controller_topic = namespace+base_name+"_"+str(x)+"_joint_position_controller"+ending
                # Add Connector Joints
                aux_connector_controller_topic = namespace+base_name+"_"+str(x)+"_aux_connector_joint_position_controller"+ending
                main_connector_controller_topic = namespace+base_name+"_"+str(x)+"_connector_joint_position_controller"+ending
        
                pub_aux = rospy.Publisher(aux_controller_topic,
                                        Float64,
                                        queue_size=1)
                pub_main = rospy.Publisher(main_controller_topic,
                                            Float64,
                                            queue_size=1)
                # Add Connector Joints
                pub_aux_connector = rospy.Publisher(aux_connector_controller_topic,
                                        Float64,
                                        queue_size=1)
                pub_main_connector = rospy.Publisher(main_connector_controller_topic,
                                            Float64,
                                            queue_size=1)
                
                self._tentacle_pub_array.append(pub_aux)
                self._tentacle_pub_array.append(pub_main)
                self._tentacle_pub_array.append(pub_aux_connector)
                self._tentacle_pub_array.append(pub_main_connector)

        print str(self._tentacle_pub_array)
    
    def move_sentinel_joints(self):
        
        alfa = 0.0
        delta = 0.2
        while not rospy.is_shutdown():
        
            
            position_command = Float64()
            position_command.data = sin(alfa)
            
            for joint_pub in self._tentacle_pub_array:
                joint_pub.publish(position_command)
            
            self._rate.sleep()
            alfa += delta
            

if __name__ == "__main__":
    number_tentacles = int(sys.argv[1])
    number_element_each_tentacle = int(sys.argv[2])
    
    sentinel_jointmover_object = SentinelJointMover(number_tentacles, number_element_each_tentacle)
    sentinel_jointmover_object.move_sentinel_joints()
