#!/usr/bin/env python
import sys
import rospy
import time
from math import pi, sin, cos, acos
import random
from std_msgs.msg import Float64



class SentinelJointMover(object):

    def __init__(self, number_tentacles):
        rospy.init_node('sentinel_clawmover', anonymous=True)
        rospy.loginfo("Sentinel Claws Initialising...")

        self._number_tentacles = number_tentacles
        
        self.init_tentacle_pub_array()
        self._rate = rospy.Rate(20)

    def init_tentacle_pub_array(self):
        
        self._tentacle_pub_array = []
        
        # /tentacle/t1_claw_finguer_base_1_joint_position_controller/command
        # /tentacle/t1_claw_finguer_tip_1_joint_position_controller/command
        namespace = "/tentacle/"
        ending = "/command"
        
        for tentacle_num in range(1, self._number_tentacles+1):
        
            base_name = "t"+str(tentacle_num)


            for finguer_number in range(1,5):
                finguer_base_controller_topic = namespace+base_name+"_claw_finguer_base_"+str(finguer_number)+"_joint_position_controller"+ending
                finguer_tip_controller_topic = namespace+base_name+"_claw_finguer_tip_"+str(finguer_number)+"_joint_position_controller"+ending
                
                pub_finguer_base = rospy.Publisher( finguer_base_controller_topic,
                                                    Float64,
                                                    queue_size=1)
                pub_finguer_tip = rospy.Publisher(  finguer_tip_controller_topic,
                                                    Float64,
                                                    queue_size=1)
                
                self._tentacle_pub_array.append(pub_finguer_base)
                self._tentacle_pub_array.append(pub_finguer_tip)
            
        print str(self._tentacle_pub_array)
    
    def move_sentinel_joints(self):
        
        alfa = 0.0
        delta = 0.05
        alfa_max = 0.0
        alfa_min = -1.57
        direction = -1.0
        
        while not rospy.is_shutdown():
        
            
            position_command = Float64()
            position_command.data = sin(alfa)
            
            for joint_pub in self._tentacle_pub_array:
                joint_pub.publish(position_command)
            
            self._rate.sleep()
            
            alfa = alfa+direction*delta
            
            if alfa <= alfa_min or alfa >= alfa_max:
                direction *= -1
                print "Change Direction"
            
            
            

if __name__ == "__main__":
    number_tentacles = int(sys.argv[1])
    
    sentinel_jointmover_object = SentinelJointMover(number_tentacles)
    sentinel_jointmover_object.move_sentinel_joints()
