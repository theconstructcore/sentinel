#!/bin/sh

elements_in_tentacles=2
number_of_tentacles=2

python config_files_yaml_generator.py $elements_in_tentacles $number_of_tentacles
python generate_tentacle_xacros.py $number_of_tentacles

number_of_elements=$(cat numelements_temp.yaml)
echo $number_of_elements
controllerargs=$(cat controllerargs_temp.yaml)
echo $controllerargs
roslaunch sentinel_description spawn_and_initcontrolers_sentinel.launch number_of_elements:="$number_of_elements" controller_args:="$controllerargs"
